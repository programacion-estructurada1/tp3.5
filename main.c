#include <iostream>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	
	  /*5.	
	  Entrada:
	  	Ingresar 30 valores.
	  
	  Proceso:
	  	 Contar
		  cuantos entre 1 y 10,     ---> cond1
		  cuantos entre 10 y 20,    ---> cond2
		  cuantos entre 20 y 30     ---> cond3 
		  y cuantos son mas de 30.  ---> cond4
	  
	  Salida:
	  	Luego mostrar el porcentaje de cada grupo en el total. */
	
	 int valor,i=0,cond1=0, cond2=0, cond3=0, cond4=0;
	 
	
	 for(i=0;i<8;i++){
	 	//entrada
	 	printf("Ingrese un valor: ");
		scanf("%d",&valor);
		
		//proceso
		if (valor>=1 ){
				 //cuantos entre 1 y 10, 
			if (valor<=10){
				cond1++;
			}
			else 
			{
				//si ingresa por esta rama, inferimos que es mayor 10.
				//cuantos entre 10 y 20
				if (valor<=20){
					cond2++;	
				}
				else
				{
					//valor si o si es mayor a 20
					if (valor<=30){
						cond3++;
					}
					else
					{
						cond4++;
					}
				}
			}
		}
	
			
	 }
	 
	 //salida: Luego mostrar el porcentaje de cada grupo en el total
	 int total=cond1+cond2+cond3+cond4; //%.2f
	 printf("El %% de la cond1 sobre el total es %d\n",(cond1*100)/total);
	 printf("El %% de la cond2 sobre el total es %d\n",(cond2*100)/total);
	 printf("El %% de la cond3 sobre el total es %d\n",(cond3*100)/total);
	 printf("El %% de la cond4 sobre el total es %d\n",(cond4*100)/total);
	return 0;
}
